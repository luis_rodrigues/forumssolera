package com.SoleraProject.Forum.ServiceTests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.SoleraProject.Forum.Entity.BadWord;
import com.SoleraProject.Forum.JpaRepository.BadWordRepository;
import com.SoleraProject.Forum.Service.BadWordService;

@RunWith(MockitoJUnitRunner.class)
class BadWordServiceTests {
	
	@InjectMocks
	private BadWordService badwordS;
	
	@Mock
	private BadWordRepository repo;
	
	
	@Test
	void testRetrieveBadWords() {
		when(repo.findAll()).thenReturn(Arrays.asList(new BadWord("duck")));
		
		List<BadWord> list = badwordS.retrieveBadWords();
		
		assertEquals("duck",list.get(0).getBadWord());
	}

}
