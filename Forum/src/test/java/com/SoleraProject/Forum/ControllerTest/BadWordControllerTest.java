package com.SoleraProject.Forum.ControllerTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.SoleraProject.Forum.Controller.BadWordController;
import com.SoleraProject.Forum.Entity.BadWord;
import com.SoleraProject.Forum.Service.BadWordService;

@RunWith(SpringRunner.class)
@WebMvcTest(BadWordController.class)
class BadWordControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private BadWordController badWordS;
	
	@Test
	void testRetrieveBadWords() throws Exception {
		when(badWordS.retrieveBadWords()).thenReturn(Arrays.asList(new BadWord("duck")));
		
		RequestBuilder request = MockMvcRequestBuilders
				.get("/badwords")
				.accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(content().json("[{badWord:duck}]"))
				.andReturn();
	}
}
