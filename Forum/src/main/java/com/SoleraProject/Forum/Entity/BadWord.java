package com.SoleraProject.Forum.Entity;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class BadWord {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonIgnore
	private int badWordId;
	
	private String badWord;
	

	public BadWord() {
		super();
	}

	public BadWord(String badWord) {
		this.badWord = badWord;
	}

	public int getBadWordId() {
		return badWordId;
	}

	public void setBadWordId(int badWordId) {
		this.badWordId = badWordId;
	}

	public String getBadWord() {
		return badWord;
	}

	public void setBadWord(String badWord) {
		this.badWord = badWord;
	}

	@Override
	public String toString() {
		return "BadWord [badWordId=" + badWordId + ", badWord=" + badWord + "]";
	}

}
