package com.SoleraProject.Forum.Entity;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
public class Forum {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int forumId;
	
	private String forumName;
	
	@OneToMany(
			mappedBy="forum"
			)
	private List<Post> listOfPosts;

	public Forum() {
		super();
	}

	public Forum(int forumId, String forumName, List<Post> listOfPosts) {
		super();
		this.forumId = forumId;
		this.forumName = forumName;
		this.listOfPosts = listOfPosts;
	}

	public int getForumId() {
		return forumId;
	}

	public void setForumId(int forumId) {
		this.forumId = forumId;
	}

	public String getForumName() {
		return forumName;
	}

	public void setForumName(String forumName) {
		this.forumName = forumName;
	}

	public List<Post> getListOfPosts() {
		return listOfPosts;
	}

	public void setListOfPosts(List<Post> listOfPosts) {
		this.listOfPosts = listOfPosts;
	}

	@Override
	public String toString() {
		return "Forum [forumId=" + forumId + ", forumName=" + forumName + ", listOfPosts=" + listOfPosts + "]";
	}

}
