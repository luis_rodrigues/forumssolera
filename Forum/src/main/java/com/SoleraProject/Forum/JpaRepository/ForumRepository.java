package com.SoleraProject.Forum.JpaRepository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SoleraProject.Forum.Entity.Forum;

public interface ForumRepository extends JpaRepository<Forum, Integer>{

}
