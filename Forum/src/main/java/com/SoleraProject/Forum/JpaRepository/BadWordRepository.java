package com.SoleraProject.Forum.JpaRepository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SoleraProject.Forum.Entity.BadWord;

public interface BadWordRepository extends JpaRepository<BadWord, Integer>{

}
