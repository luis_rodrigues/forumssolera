package com.SoleraProject.Forum.JpaRepository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SoleraProject.Forum.Entity.Post;

public interface PostRepository extends JpaRepository<Post, Integer>{

}
