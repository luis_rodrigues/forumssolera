package com.SoleraProject.Forum.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.SoleraProject.Forum.Entity.Post;
import com.SoleraProject.Forum.Service.PostService;

import jakarta.validation.Valid;

@RestController
@CrossOrigin
public class PostController {

	@Autowired
	private PostService postS;
	
	//////POST
	@GetMapping(path="/forums/{forumId}/posts")
	public List<Post> retrieveAllPost (@PathVariable int forumId){
		return postS.retrieveAllPost(forumId);
	}
	
	
	@GetMapping(path="/posts/{postId}")
	public Post retrieveAPost (@PathVariable int postId) {
		return postS.retrieveAPost(postId);
	}
	
	@PostMapping(path="/forums/{forumId}/posts")
	public ResponseEntity<Post> createPost(@Valid @RequestBody Post post, @PathVariable int forumId){
		return postS.createPost(post, forumId);
	}
	
	@PutMapping(path="/posts/{postId}")
	public ResponseEntity<Post> updatePost(@Valid @RequestBody Post post,@PathVariable int postId){
		return postS.updatePost(post, postId);
	}
	
	@DeleteMapping(path="/posts/{postId}")
	public void deletePostById(@PathVariable int postId) {
		postS.deletePostById(postId);
	}
	
	
}
