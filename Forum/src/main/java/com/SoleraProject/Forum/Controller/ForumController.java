package com.SoleraProject.Forum.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.SoleraProject.Forum.Entity.Forum;
import com.SoleraProject.Forum.Service.ForumService;

import jakarta.validation.Valid;

@RestController
@CrossOrigin
public class ForumController {
	
	@Autowired
	private ForumService forumS;
	
	@GetMapping(path="/forums")
	public List<Forum> retrieveAllForums (){
		return forumS.retrieveAllForums();	
	}
	
	@PostMapping(path="/forums")
	public ResponseEntity<Forum> createForum(@Valid @RequestBody Forum forum){
		return forumS.createForum(forum);
	}
}
