package com.SoleraProject.Forum.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.SoleraProject.Forum.Entity.BadWord;
import com.SoleraProject.Forum.Entity.Post;
import com.SoleraProject.Forum.Service.BadWordService;

@RestController
@CrossOrigin
public class BadWordController {

	@Autowired
	private BadWordService badWordS;
	
	//////BADWORDS
	
	@GetMapping(path="/badwords")
	public List<BadWord> retrieveBadWords (){
		return badWordS.retrieveBadWords();
	}
	
	@PostMapping(path="/badwords")
	public ResponseEntity<Post> createBadWords(@RequestBody BadWord word){
		return badWordS.createBadWords(word);
	}
	
	
}
