package com.SoleraProject.Forum.Service;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.SoleraProject.Forum.Entity.BadWord;
import com.SoleraProject.Forum.Entity.Forum;
import com.SoleraProject.Forum.Entity.Post;
import com.SoleraProject.Forum.JpaRepository.BadWordRepository;
import com.SoleraProject.Forum.JpaRepository.ForumRepository;
import com.SoleraProject.Forum.JpaRepository.PostRepository;

import jakarta.validation.Valid;

@Component
public class PostService {
	
	@Autowired
	private ForumRepository forumS;
	@Autowired
	private PostRepository postS;
	@Autowired
	private BadWordRepository badWordS;
	
	public List<Post> retrieveAllPost (int forumId){
		Forum getForum = forumS.findById(forumId).get();
		return getForum.getListOfPosts();
	}
	
	public Post retrieveAPost (int postId){
		return postS.findById(postId).get();
	}
	
	public ResponseEntity<Post> createPost(Post post, int forumId){
		Forum getForum = forumS.findById(forumId).get();
		
		List<Post> listPost = getForum.getListOfPosts();
		
		//check if the title exists
		for(Post elem : listPost) {
			if(post.getTitle().equals(elem.getTitle())) {
				return ResponseEntity.badRequest().build();
			}
		}
		
		List<BadWord> listBadWord = badWordS.findAll();
		
		//check if the body as a bad word;
		for(BadWord elem : listBadWord) {
			String str = elem.getBadWord().toLowerCase();
			
			if(post.getBody().contains(str)) {
				return ResponseEntity.badRequest().build();
			}
		}
		
		post.setForum(getForum);
		Post savedPost = postS.save(post);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/id")
				.buildAndExpand(savedPost.getPostId())
				.toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	public ResponseEntity<Post> updatePost(Post post,int postId){
		Post getPost = postS.findById(postId).get();
		
		getPost.setTitle(post.getTitle());
		getPost.setBody(post.getBody());
		getPost.setImage(post.getImage());
		getPost.setCategory(post.getCategory());
		
		Post savedPost = postS.save(getPost);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/id")
				.buildAndExpand(savedPost.getPostId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	public void deletePostById(int postId) {
		Post getPost = postS.findById(postId).get();
		postS.delete(getPost);
	}
}
