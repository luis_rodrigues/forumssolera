package com.SoleraProject.Forum.Service;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.SoleraProject.Forum.Entity.BadWord;
import com.SoleraProject.Forum.Entity.Post;
import com.SoleraProject.Forum.JpaRepository.BadWordRepository;

@Component
public class BadWordService {
	
	@Autowired
	private BadWordRepository badWordS;
	
	public List<BadWord> retrieveBadWords (){
		return badWordS.findAll();
	}
	
	public ResponseEntity<Post> createBadWords(BadWord word){
		
		BadWord savedBadWord = badWordS.save(word);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/id")
				.buildAndExpand(savedBadWord.getBadWordId())
				.toUri();
		
		return ResponseEntity.created(location).build();
	}
}
