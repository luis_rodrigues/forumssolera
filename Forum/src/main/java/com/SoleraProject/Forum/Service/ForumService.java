package com.SoleraProject.Forum.Service;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.SoleraProject.Forum.Entity.Forum;
import com.SoleraProject.Forum.JpaRepository.ForumRepository;

import jakarta.validation.Valid;

@Component
public class ForumService {
	
	@Autowired
	private ForumRepository forumS;


	public List<Forum> retrieveAllForums (){
		return forumS.findAll();	
	}
	
	public ResponseEntity<Forum> createForum(Forum forum){
		Forum savedForum = forumS.save(forum);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/id")
				.buildAndExpand(savedForum.getForumId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

}
